# Contributor: Anjandev Momi <anjan@momi.ca>
# Maintainer: Anjandev Momi <anjan@momi.ca>
pkgname=river
pkgver=0.3.4
pkgrel=0
pkgdesc="Dynamic Tiling Wayland Compositor"
url="https://codeberg.org/river/river"
arch="x86_64 aarch64" # limited by zig aport
license="GPL-3.0-only"
makedepends="
	libevdev-dev
	libxkbcommon-dev
	pixman-dev
	scdoc
	wayland-dev
	wayland-protocols
	wlroots-dev
	zig
	"
depends="xwayland seatd"
subpackages="
	$pkgname-dev
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
options="net"
source="https://codeberg.org/river/river/releases/download/v$pkgver/river-$pkgver.tar.gz"

# We may want other than "baseline" for other targets, when enabled by zig
case "$CARCH" in
	aarch64|x86_64) cputarget=baseline ;;
esac

build() {
	# This installs it to $builddir/out
	DESTDIR="$builddir/out" zig build \
		-Doptimize=ReleaseSafe \
		-Dpie \
		-Dxwayland \
		--prefix /usr install \
		${cputarget:+-Dcpu="$cputarget"}
}

check() {
	zig build test
}

package() {
	mkdir -p "$pkgdir"
	cp -r out/* "$pkgdir"

	# Fix location of pkgconfig files, must be fixed upstream
	mkdir -p "$pkgdir"/usr/lib
	mv "$pkgdir"/usr/share/pkgconfig "$pkgdir"/usr/lib

	# Install example configuration
	install -Dm0644 example/init -t "$pkgdir"/usr/share/doc/river/examples
}

dev() {
	default_dev
	amove usr/share/river-protocols
}

sha512sums="
857cdc9e170b066ea4e0166d86ee6ebdf8e0bbe555b6d4fe390f6e92664daa3d4134dc2e1c07c35ff21e2bf7f75698b9a06cbf20c9e210ba3609dda2fd4ea0b9  river-0.3.4.tar.gz
"
