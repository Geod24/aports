# Contributor: Celeste <cielesti@protonmail.com>
# Maintainer: Celeste <cielesti@protonmail.com>
pkgname=mitra
pkgver=2.25.1
pkgrel=0
_mitraweb="${pkgver%.*}.0"
pkgdesc="ActivityPub microblogging platform written in Rust"
url="https://mitra.social/@mitra"
# riscv64: vite webapp fails to build: 'Parse error @:1:38'
# loongarch64: blocked by libc crate
arch="all !riscv64 !loongarch64"
license="AGPL-3.0-only"
depends="postgresql"
makedepends="
	cargo
	cargo-auditable
	nodejs
	npm
	openssl-dev
	"
install="$pkgname.pre-install $pkgname.post-install"
pkgusers="mitra"
pkggroups="mitra"
subpackages="$pkgname-doc $pkgname-openrc"
source="mitra-$pkgver.tar.gz::https://codeberg.org/silverpill/mitra/archive/v$pkgver.tar.gz
	mitra-web-$_mitraweb.tar.gz::https://codeberg.org/silverpill/mitra-web/archive/v$_mitraweb.tar.gz
	mitra.initd
	init.sql
	config.yaml
	native-tls.patch
	"
builddir="$srcdir/mitra"

# use system openssl
export OPENSSL_NO_VENDOR=1

_cargo_opts="--frozen --no-default-features --features production,native-tls"

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked

	cd "$srcdir/mitra-web"

	npm ci --foreground-scripts
}

build() {
	RUSTFLAGS="--cfg=ammonia_unstable" \
	cargo auditable build $_cargo_opts --release

	cd "$srcdir/mitra-web"

	echo 'VITE_BACKEND_URL=' > .env.local
	npm run build
}

check() {
	# These tests require a database connection
	RUSTFLAGS="--cfg=ammonia_unstable" \
	cargo test $_cargo_opts --workspace \
		--exclude mitra_models -- \
		--skip test_follow_unfollow \
		--skip test_hide_reblogs \
		--skip test_subscribe_unsubscribe \
		--skip test_get_jrd \
		--skip test_filter_mentions_none \
		--skip test_filter_mentions_only_known \
		--skip test_prepare_instance_ed25519_key \
		--skip test_mute

	cd "$srcdir/mitra-web"

	npm run test
}

package() {
	install -Dm755 target/release/mitra -t "$pkgdir"/usr/bin
	install -Dm755 target/release/mitractl -t "$pkgdir"/usr/bin

	mkdir -p "$pkgdir"/usr/share/webapps
	cp -r "$srcdir"/mitra-web/dist \
		"$pkgdir"/usr/share/webapps/mitra

	install -Dm644 docs/* -t "$pkgdir"/usr/share/doc/$pkgname
	install -Dm644 config_dev.example.yaml \
		contrib/Caddyfile contrib/*.nginx \
		contrib/monero/wallet.conf \
		-t "$pkgdir"/usr/share/doc/$pkgname/examples

	install -Dm640 -g mitra "$srcdir"/config.yaml -t "$pkgdir"/etc/mitra
	install -dm755 -o mitra -g mitra "$pkgdir"/var/lib/mitra
	install -Dm644 "$srcdir"/init.sql -t "$pkgdir"/var/lib/mitra
	install -Dm755 "$srcdir"/mitra.initd "$pkgdir"/etc/init.d/mitra
}

sha512sums="
27a8f3e82dc7bef32288f6b214b4fd2f064f3e3f0eadafafbff9431e17133d3916b15af9a81412841ba622aeac1089b819394d344bb9c26a63f5e65a2e2de12d  mitra-2.25.1.tar.gz
b64199e44e3dd9d23af51401cbf1ec808482b3bf559ebfbd336119a0c19061b61c1bc2f49b37b48005db23fd9c79555a65fa1187ac2b71cd43a2e7a19de8a647  mitra-web-2.25.0.tar.gz
691f84f5dfdddc176e75792ab03ff167054246e75ced51be47a89f405ae55ebe5eb6280b73c1b467b5ecbe8539f6108fb3d86873d50fcc4f4b8c5b182632acb0  mitra.initd
180a47f5072534418b4aac3ce7c885a4f7e4dc38aca80d6d81c79848d12fbe24799788c3575bd195030a10da5e0372f87fa2809a4ef99a48eaa6df52f4d053dd  init.sql
315a14179cd2cf561cc595f8c97fa1e39d714a39f95f50e09c05525e7103999de68ef33a7d47a9a0db82493de89e45e6379ea3771fa70289d2b4c60d0ee50ba8  config.yaml
53169430a6ab5a97d069dff1669e506fd02c9e6da663b657c061097e7f80647dcf4fc6edc5d135ddcb898fb64958e169a7d9cd08e83e60add8f15af7784e273f  native-tls.patch
"
