# Contributor: Patrycja Rosa <alpine@ptrcnull.me>
# Maintainer: Patrycja Rosa <alpine@ptrcnull.me>
pkgname=py3-dep-logic
pkgver=0.3.3
pkgrel=0
pkgdesc="Python dependency specifications supporting logical operations"
url="https://github.com/pdm-project/dep-logic"
arch="noarch"
license="Apache-2.0"
depends="py3-packaging"
makedepends="py3-gpep517 py3-pdm-backend py3-wheel"
checkdepends="py3-pytest"
subpackages="$pkgname-pyc"
source="https://github.com/pdm-project/dep-logic/archive/$pkgver/py3-dep-logic-$pkgver.tar.gz"
builddir="$srcdir/dep-logic-$pkgver"

build() {
	export PDM_BUILD_SCM_VERSION=$pkgver
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
cb8a89692ee7ca471c9f953fade34051995f898817a4de499ce6fc64ce0462b240a81be2fbced0d0aefdeec608d2842fe2030033321a53984dfc7ad73f8bbaf2  py3-dep-logic-0.3.3.tar.gz
"
