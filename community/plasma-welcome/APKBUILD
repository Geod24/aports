# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=plasma-welcome
pkgver=6.1.2
pkgrel=0
pkgdesc="A friendly onboarding wizard for Plasma"
# armhf blocked by qt6-qtdeclarative
# ppc64le, s390x, riscv64 and loongarch64 blocked by qt6-qtwebengine -> kaccounts-integration
arch="all !armhf !ppc64le !s390x !riscv64 !loongarch64"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later"
depends="kirigami"
makedepends="
	extra-cmake-modules
	kcmutils-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kdbusaddons-dev
	kdeclarative-dev
	ki18n-dev
	kio-dev
	kirigami-addons-dev	
	kirigami-dev
	knewstuff-dev
	knotifications-dev
	kservice-dev
	ksvg-dev
	kuserfeedback-dev
	kwindowsystem-dev
	libplasma-dev
	qt6-qtbase-dev
	qt6-qtdeclarative-dev
	qt6-qtsvg-dev
	samurai
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
_repo_url="https://invent.kde.org/plasma/plasma-welcome.git"
source="https://download.kde.org/stable/plasma/$pkgver/plasma-welcome-$pkgver.tar.xz"
subpackages="$pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
3bf205eb5f2f38b1a10a3d569a781bdb8294b6112b7fccc3c37d6eafb48ac51ed72cdf2e1b73684c03ebd2aa98c3783f18e20ab5de86c4ebbaf167f7339a499c  plasma-welcome-6.1.2.tar.xz
"
