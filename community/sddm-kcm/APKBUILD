# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=sddm-kcm
pkgver=6.1.2
pkgrel=0
pkgdesc="Config module for SDDM"
# armhf blocked by extra-cmake-modules
# s390x blocked by systemsettings
arch="all !armhf !s390x"
url="https://www.kde.org"
license="GPL-2.0-or-later AND (GPL-2.0-only OR GPL-3.0-only) AND LGPL-2.1-or-later AND GPL-2.0-only"
depends="
	sddm
	systemsettings
	"
makedepends="
	extra-cmake-modules
	karchive-dev
	kauth-dev
	kcmutils-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kdeclarative-dev
	ki18n-dev
	kio-dev
	knewstuff-dev
	kxmlgui-dev
	libxcursor-dev
	qt6-qtbase-dev
	qt6-qtdeclarative-dev

	samurai
	xcb-util-image-dev
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
subpackages="$pkgname-lang"
_repo_url="https://invent.kde.org/plasma/sddm-kcm.git"
source="https://download.kde.org/stable/plasma/$pkgver/sddm-kcm-$pkgver.tar.xz"
# No tests
options="!check"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
d2578e1ff92b941864f79ea4f6c7c6d036cf158b4fe9e045b4302c197cf54ac8d9f7e1fe02da36943957b9a69a5095bcc82165ee14318db1372f6dcccd9d7b89  sddm-kcm-6.1.2.tar.xz
"
