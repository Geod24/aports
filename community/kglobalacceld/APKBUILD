# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=kglobalacceld
pkgver=6.1.2
pkgrel=0
pkgdesc="Daemon providing Global Keyboard Shortcut (Accelerator) functionality"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://invent.kde.org/plasma/kglobalacceld"
license="LGPL-2.0-or-later AND LGPL-2.0-only AND LGPL-2.1-or-later"
depends_dev="$pkgname
	kconfig-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kglobalaccel-dev
	kio-dev
	knotifications-dev
	kservice-dev
	kwindowsystem-dev
	qt6-qtbase-dev
	xcb-util-keysyms-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	graphviz
	qt6-qttools-dev
	samurai
	"
checkdepends="
	dbus
	xvfb-run
	"
subpackages="$pkgname-dev"
_repo_url="https://invent.kde.org/plasma/kglobalacceld.git"
source="https://download.kde.org/stable/plasma/$pkgver/kglobalacceld-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	dbus-run-session -- xvfb-run -a ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
3af63542a54e9cb01dd5d427c9608d8495535a0895345976e3acfa493d544d5cc68a23ceaab1e007a8ce6203b023393aff984b903a05c54bb3ac3b79c8f04b7c  kglobalacceld-6.1.2.tar.xz
"
