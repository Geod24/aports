# Contributor: Bart Ribbers <bribbers@disroot.org>
# Contributor: Danct12 <danct12@disroot.org>
# Contributor: Clayton Craft <clayton@craftyguy.net>
# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: team/phosh <newbie13xd@gmail.com>
pkgname=phoc
pkgver=0.40.0
pkgrel=1
pkgdesc="wlroots based Phone compositor for the Phosh shell"
arch="all !s390x" # blocked by gnome-desktop
url="https://gitlab.gnome.org/World/Phosh/phoc"
license="GPL-3.0-only"
depends="
	dbus
	mutter-schemas
	gsettings-desktop-schemas
	"
_wlrootsmakedepends="
	eudev-dev
	hwdata-dev
	libcap-dev
	libdisplay-info-dev
	libseat-dev
	libxcb-dev
	xcb-util-image-dev
	xcb-util-renderutil-dev
	xcb-util-wm-dev
	xkeyboard-config-dev
	xwayland-dev
	"
makedepends="
	glib-dev
	gmobile-dev
	gnome-desktop-dev
	json-glib-dev
	libdrm-dev
	libinput-dev
	libxkbcommon-dev
	mesa-dev
	meson
	pixman-dev
	wayland-dev
	wayland-protocols
	$_wlrootsmakedepends
	"
checkdepends="xvfb-run"
subpackages="$pkgname-dbg"
options="!check" # Needs fullblown EGL
source="https://sources.phosh.mobi/releases/phoc/phoc-${pkgver/_/.}.tar.xz
	fix-crash-caused-by-squeekboard.patch
	"
replaces="wlroots-phosh"
builddir="$srcdir/$pkgname-${pkgver/_/.}"

prepare() {
	default_prepare

	patch -Np1 < subprojects/packagefiles/wlroots/0001-Revert-layer-shell-error-on-0-dimension-without-anch.patch -d subprojects/wlroots
}

build() {
	abuild-meson \
		-Db_lto=true \
		-Dembed-wlroots=enabled \
		-Dtests="$(want_check && echo true || echo false)" \
		--default-library=static \
		. output
	meson compile -C output
}

check() {
	xvfb-run -a meson test --no-rebuild --print-errorlogs -C output
}

package() {
	DESTDIR="$pkgdir/" meson install --no-rebuild -C output
	install -Dm755 helpers/scale-to-fit \
		-t "$pkgdir"/usr/bin

	# remove unneeded wlroots dev. files
	rm -r "$pkgdir"/usr/include
	rm -r "$pkgdir"/usr/lib/libwlroots.a
	rm -r "$pkgdir"/usr/lib/pkgconfig
}

sha512sums="
3d3223ca7f2d3e865c3aa7924447d638b9dd7fef054f71ce667d545d1eeb5d13082e87093adc654426e7dec44e28df41858ef03bb3bb7d0c6c815e1bc958c855  phoc-0.40.0.tar.xz
7b281c3d8de48d6189e875502e5d93b0d2c619ad64d7e1068ba7203b4a94cb960cccc30c5efaaf48ceecf5b9a84afc53e053c5a6d0fe119a549e9e2c5a9040b0  fix-crash-caused-by-squeekboard.patch
"
