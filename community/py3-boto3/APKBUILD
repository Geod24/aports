# Contributor: Valery Kartel <valery.kartel@gmail.com>
# Maintainer: Antoine Martin (ayakael) <dev@ayakael.net>
pkgname=py3-boto3
pkgver=1.34.139
pkgrel=0
pkgdesc="AWS SDK for Python (Boto3)"
url="https://aws.amazon.com/sdk-for-python/"
license="Apache-2.0"
arch="noarch"
depends="
	py3-botocore
	py3-jmespath
	py3-s3transfer
	"
makedepends="
	py3-gpep517
	py3-setuptools
	py3-wheel
	"
subpackages="$pkgname-pyc"
source="https://files.pythonhosted.org/packages/source/b/boto3/boto3-$pkgver.tar.gz"
builddir="$srcdir"/boto3-$pkgver
options="!check" # take way too long

replaces="py-boto3" # Backwards compatibility
provides="py-boto3=$pkgver-r$pkgrel" # Backwards compatibility

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
cd8c0a39b29d0a77c79fb09c1a13c59aa66f3531db9d63f4d7a17267c76523f5d983af28538cfd2f1224de2c6e97bd60f525ff1525df793730843d2d93c8daa4  boto3-1.34.139.tar.gz
"
