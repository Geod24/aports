# Contributor: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
# Maintainer: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
pkgname=py3-niaarm
_pkgorig=niaarm
pkgver=0.3.10
pkgrel=1
pkgdesc="A minimalistic framework for numerical association rule mining"
url="https://github.com/firefly-cpp/NiaARM"
arch="noarch"
license="MIT AND Apache-2.0"
depends="python3 py3-numpy py3-pandas py3-niapy py3-nltk py3-scikit-learn"
checkdepends="py3-pytest-xdist"
makedepends="py3-gpep517 py3-poetry-core"
subpackages="$pkgname-doc $pkgname-pyc"
source="https://github.com/firefly-cpp/NiaARM/archive/$pkgver/$_pkgorig-$pkgver.tar.gz"
builddir="$srcdir/NiaARM-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest -n auto -k 'not text_mining' --ignore=tests/test_visualization.py
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/niaarm-$pkgver-py3-none-any.whl

	install -Dm644 paper/10.21105.joss.04448.pdf -t "$pkgdir"/usr/share/doc/$pkgname
	install -Dm644 CITATION.cff -t "$pkgdir"/usr/share/doc/$pkgname
	install -Dm644 interest_measures.md -t "$pkgdir"/usr/share/doc/$pkgname

	install -Dm644 niaarm.1 -t "$pkgdir"/usr/share/man/man1/
}

sha512sums="
b5243a1413c6d1589773a1442c9e15bba5534b6ddcb9136bcf9abf3f55e7bd5e93e702082c5a4c1979bda38e21a7932c7129a2d3819b5aaffadc02104d078a28  niaarm-0.3.10.tar.gz
"
