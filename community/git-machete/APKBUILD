# Contributor: Kevin Daudt <kdaudt@alpinelinux.org>
# Maintainer: Kevin Daudt <kdaudt@alpinelinux.org>
pkgname=git-machete
pkgver=3.26.2
pkgrel=0
pkgdesc="git repository organizer & rebase/merge workflow automation tool"
url="https://github.com/VirtusLab/git-machete"
arch="noarch"
license="MIT"
depends="python3"
makedepends="py3-gpep517 py3-setuptools py3-wheel"
checkdepends="py3-pytest py3-pytest-mock zsh bash fish bash-completion"
subpackages="
	$pkgname-pyc
	$pkgname-bash-completion
	$pkgname-zsh-completion
	$pkgname-fish-completion
	"
source="https://github.com/VirtusLab/git-machete/archive/refs/tags/v$pkgver/git-machete-$pkgver.tar.gz
	"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	PATH="$PATH:$builddir/.testenv/bin" EDITOR=cat pytest -k 'not TestCompletionEndToEnd'
}

package() {
	python3 -m installer -d "$pkgdir" .dist/*.whl

	install -Dm0644 "$builddir"/completion/git-machete.completion.bash \
		"$pkgdir/usr/share/bash-completion/completions/git-machete"
	install -Dm0644 "$builddir"/completion/git-machete.completion.zsh \
		"$pkgdir/usr/share/zsh/site-functions/_git-machete"
	install -Dm0644 "$builddir"/completion/git-machete.fish \
		-t "$pkgdir/usr/share/fish/vendor_completions.d"
}

sha512sums="
1d12e59db3b09203531f8cfe50d06ac278db90691eb8f26b859d29baa4cc6166f21a9842d74a9f3f34c599d5762d48613dac34d30e0db37e3716c36f52b8d962  git-machete-3.26.2.tar.gz
"
